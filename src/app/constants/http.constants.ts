import { HttpHeaders } from "@angular/common/http";

const SERVER_URL: string = 'http://localhost:3000/api';

const BASIC_HTTP_OPTIONS = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
} as const;

export { SERVER_URL, BASIC_HTTP_OPTIONS };