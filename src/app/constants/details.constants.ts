export const COLORS = [
    'red',
    'blue',
    'yellow',
    'green',
    'black'
] as const;

export const SIZES = [
    'XS',
    'S',
    'M',
    'L',
    'XL',
    'XXL'
] as const;