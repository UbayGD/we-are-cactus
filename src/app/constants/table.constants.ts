
export const COLUMNS_NAMES = {
    NAME: 'name',
    DESCRIPTION: 'description',
    COLOR: 'color',
    SIZE: 'size',
    STATUS: 'status',
    CREATE_AT: 'created_at',
    UPDATED_AT: 'updated_at',
} as const;

export const COLUMNS_DEFINITIONS = Object.values(COLUMNS_NAMES).map((value) => {
    return {
        columnDef: value,
        header: value.toLocaleUpperCase(),
        modelProperty: value
    }      
})