import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { COLORS, SIZES } from 'src/app/constants/details.constants';
import { ProductModel } from 'src/app/models/product.model';
import { ToggleService } from 'src/app/services/toggle/toggle.service';

@Component({
  selector: 'detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  colors = COLORS;
  sizes = SIZES;
  
  @Input() product!: ProductModel;
  @Output() productChange = new EventEmitter<ProductModel>();

  constructor(private toggleService: ToggleService) { }

  ngOnInit(): void { }

  changeStatus($event: MatSlideToggleChange) {
    this.product.status = $event.checked ? 'active' : 'inactive'
  }

  getStatus(): boolean {
    return this.product.status === 'active';
  }

  getDate() {
    return new Date().toISOString().split('T')[0].replaceAll('-','/');
  }

  toggleDrawer() {
    this.toggleService.toggle();
  }

  close() {
    this.product.updated_at = this.getDate();
    this.toggleDrawer();
    this.productChange.emit(this.product);
  }

}
