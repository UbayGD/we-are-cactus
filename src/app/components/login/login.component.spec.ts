import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { AuthService } from 'src/app/services/auth/auth.service';

import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [
        BrowserAnimationsModule,
        HttpClientModule, 
        AppRoutingModule, 
        JwtModule.forRoot({
          config: {
            tokenGetter: () => {
              return localStorage.getItem("token");
            },
          },
        }),
        MatFormFieldModule,
        MatInputModule,
        MatCardModule,
        MatIconModule,
        ReactiveFormsModule
      ],
      providers: [
        LoginComponent,
        AuthService,
        JwtHelperService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Show errors when inputs are touched without add any value', () => {
    const email = component.loginForm.controls['email'];
    const password = component.loginForm.controls['password'];
    
    email.markAsTouched();
    password.markAsTouched();

    fixture.detectChanges();

    let emailError = fixture.nativeElement.querySelector('#email-error');
    let passwordError = fixture.nativeElement.querySelector('#password-error');

    expect(emailError.textContent).toEqual('You must enter an email');
    expect(passwordError.textContent).toEqual('You must enter a password');
  });

  it('Enable SignIn button when form is correctly filled', () => {
    const email = component.loginForm.controls['email'];
    const password = component.loginForm.controls['password'];
    
    email.setValue('test@wearecactus.com');
    email.markAsTouched();

    password.setValue('1984');
    password.markAsTouched();

    fixture.detectChanges();

    const signinButton = fixture.nativeElement.querySelector('button[type=submit]');    
    expect(signinButton.disabled).toBeFalsy();
  });
});
