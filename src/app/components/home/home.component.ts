import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductModel } from 'src/app/models/product.model';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ProductService } from 'src/app/services/product/product.service';
import { COLUMNS_NAMES, COLUMNS_DEFINITIONS } from 'src/app/constants/table.constants';
import { MatDrawer } from '@angular/material/sidenav/drawer';
import { ToggleService } from 'src/app/services/toggle/toggle.service';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  columnsNames = COLUMNS_NAMES;
  columnsDefinition = COLUMNS_DEFINITIONS;

  products: ProductModel[] | undefined = undefined;
  selectedProduct: ProductModel | undefined = undefined;

  @ViewChild('drawer', {static: true}) drawer!: MatDrawer;

  constructor(private authService: AuthService, private productService: ProductService, private toggleService: ToggleService) { }

  ngOnInit(): void {
    this.productService.getProducts().subscribe((products) => {
      this.products = products
    }); 
  }

  ngAfterViewInit() {
    this.toggleService.setDrawer(this.drawer); 
  }

  logout() {
    this.authService.logout();
  }

  onClickRow(product: ProductModel) {
    // i'm not proud of this
    if (this.selectedProduct) {
      if (this.selectedProduct.id !== product.id) {
        this.setSelectedProduct(product);
        this.toggleDrawer();
      } else {
        this.toggleDrawer();
      }
    } else {
      this.setSelectedProduct(product);
      this.toggleDrawer();
    }
  }

  toggleDrawer() {
    if (!this.toggleService.isOpened()) this.toggleService.toggle();
  }

  private setSelectedProduct(product: ProductModel | undefined) {
    this.selectedProduct = product;
  }

}
