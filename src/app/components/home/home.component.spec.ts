import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { AuthService } from 'src/app/services/auth/auth.service';

import { HomeComponent } from './home.component';
import { ProductService } from 'src/app/services/product/product.service';
import { ProductModel } from 'src/app/models/product.model';
import { of } from 'rxjs';
import { MatTableModule } from '@angular/material/table';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let productService: ProductService;
  let element;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      imports: [
        BrowserAnimationsModule,
        HttpClientModule,
        AppRoutingModule,
        JwtModule.forRoot({
          config: {
            tokenGetter: () => {
              return localStorage.getItem("token");
            },
          },
        }),
        MatIconModule,
        MatSidenavModule,
        MatToolbarModule,
        MatTableModule,
        MatProgressSpinnerModule
      ],
      providers: [
        HomeComponent,
        AuthService,
        ProductService,
        JwtHelperService
      ]
    })
    .compileComponents();
  });

  beforeEach(inject([ProductService], (s: ProductService) => {
    productService = s;
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
  }));

  it("should show a table with products", () => {
    const response: ProductModel[] = [
      {
        id: 0,
        name: 'test 1',
        color: 'black',
        size: 'XL',
        status: 'active',
        description: 'Test Product 1',
        created_at: '2021/06/24',
        updated_at: '2021/06/24'
      },
      {
        id: 1,
        name: 'test 2',
        color: 'black',
        size: 'XL',
        status: 'active',
        description: 'Test Product 2',
        created_at: '2021/06/24',
        updated_at: '2021/06/24'
      }
    ];

    spyOn(productService, 'getProducts').and.returnValue(of(response))
    component.ngOnInit();
    fixture.detectChanges();

    expect(component.products).toEqual(response);

    let table: HTMLElement = fixture.nativeElement.querySelector('.mat-table');
    expect(table).toBeDefined();
  });
});
