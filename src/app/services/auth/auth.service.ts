import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JwtHelperService }  from '@auth0/angular-jwt';
import { BASIC_HTTP_OPTIONS, SERVER_URL } from 'src/app/constants/http.constants';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  constructor(private http: HttpClient, private router: Router, private jwtHelper: JwtHelperService) { }

  login(email: string, password: string): Observable<any> {    
    return this.http.post<any>(`${SERVER_URL}/login`, {email, password}, BASIC_HTTP_OPTIONS);
  }

  setToken(token: string) {
    localStorage.setItem('token', token);
  }
  
  logout() {
    localStorage.removeItem("token");
    this.router.navigate(['login']);
  }

  public isAuthenticated(): boolean {    
    const token = localStorage.getItem('token');
    return token ? !this.jwtHelper.isTokenExpired(token) : false;
  }
 
}
