import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BASIC_HTTP_OPTIONS, SERVER_URL } from 'src/app/constants/http.constants';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  getProducts(): Observable<any> {
    let headers = BASIC_HTTP_OPTIONS.headers.append('Authorization', `Bearer ${localStorage.getItem('token')}`);    
    return this.http.get<any>(`${SERVER_URL}/products`, {
      headers
    });
  }
}
