# WeAreCactus

<img src="src/assets/wearecactus.jpeg">

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.0.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to executeS the unit tests via [Karma](https://karma-runner.github.io).

## Deploy in Docker

Run `sh build.sh` and `sh run.sh`. The result will be a container called 'we-are-cactus-front-container', which will be accessible from [localhost:80](http://localhost:80)