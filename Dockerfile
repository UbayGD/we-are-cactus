### STAGE 1: Build ###
FROM node:latest AS build
WORKDIR /usr/local/app
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN npm run build

### STAGE 2: Run ###
FROM nginx:1.21.0
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build /usr/local/app/dist/we-are-cactus /usr/share/nginx/html